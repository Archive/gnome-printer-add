/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*
 *  Authors: Jeffrey Stedfast <fejj@novell.com>
 *
 *  Copyright 2005 Novell, Inc. (www.novell.com)
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Street #330, Boston, MA 02111-1307, USA.
 *
 */


#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <stdio.h>
#include <string.h>

#include <libgnomecups/gnome-cups-init.h>
#include <libgnomecups/gnome-cups-printer.h>

static int num_listeners = 0;
static GMainLoop *main_loop = NULL;
static GSList *printers = NULL;
static gboolean found = FALSE;

static void
configure_printer (const char *uri)
{
	char *argv[6];
	
	argv[0] = "/bin/sh";
	argv[1] = "-c";
	argv[2] = "gnome-cups-add";
	argv[3] = "--printer";
	argv[4] = uri;
	argv[5] = NULL;
	
	g_spawn_async (g_get_home_dir (), argv, NULL, 0, NULL, NULL, NULL, NULL);
}

static void
attributes_changed_cb (GnomeCupsPrinter *printer, char *argv_uri)
{
	const char *uri;
	
	uri = gnome_cups_printer_get_device_uri (printer);
	if (!strcmp (uri, argv_uri)) {
		g_main_loop_quit (main_loop);
		found = TRUE;
	}
	
	num_listeners--;
	if (num_listeners == 0)
		g_main_loop_quit (main_loop);
}

static void
check_printer (const char *name, char *argv_uri)
{
	GnomeCupsPrinter *printer;
	
	printer = gnome_cups_printer_get (name);
	if (!gnome_cups_printer_get_attributes_initialized (printer)) {
		printers = g_slist_prepend (printers, printer);
		g_signal_connect (printer, "attributes-changed",
				  G_CALLBACK (attributes_changed_cb),
				  argv_uri);
		num_listeners++;
	} else {
		attributes_changed_cb (printer, argv_uri);
	}
}

static const char *
basename (const char *path)
{
	const char *name;
	
	return (name = strrchr (path, '/')) ? name + 1 : path;
}

int main (int argc, char **argv)
{
	GSList *n;
	
	if (argc != 2) {
		printf ("Usage: %s [printer URI]\n", basename (argv[0]));
		return 1;
	}
	
	gnome_cups_printer_new_printer_notify_add (check_printer, argv[1]);
	
	gnome_cups_init (NULL);
	
	main_loop = g_main_loop_new (NULL, FALSE);
	g_main_loop_run (main_loop);
	g_main_loop_unref (main_loop);
	
	gnome_cups_shutdown ();
	
	while (printers) {
		n = printers->next;
		g_object_unref (printers->data);
		g_slist_free_1 (printers);
		printers = n;
	}
	
	if (!found)
		configure_printer (argv[1]);
	
	return 0;
}
